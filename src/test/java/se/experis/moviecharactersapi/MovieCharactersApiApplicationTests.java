package se.experis.moviecharactersapi;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mapstruct.factory.Mappers;
import org.springframework.boot.test.context.SpringBootTest;
import se.experis.moviecharactersapi.dto.character.CharacterDTO;
import se.experis.moviecharactersapi.dto.franchise.FranchiseDTO;
import se.experis.moviecharactersapi.dto.movie.MovieDTO;
import se.experis.moviecharactersapi.mapper.CharacterMapper;
import se.experis.moviecharactersapi.mapper.FranchiseMapper;
import se.experis.moviecharactersapi.model.Character;
import se.experis.moviecharactersapi.model.Franchise;

import java.util.HashSet;
import java.util.Set;

@SpringBootTest
class MovieCharactersApiApplicationTests {

    private CharacterMapper mapper = Mappers.getMapper(CharacterMapper.class);
    private FranchiseMapper franchiseMapper = Mappers.getMapper(FranchiseMapper.class);

    @Test
    void givCharacterDTONameToCharacter_WhenMaps_thenCorrect() {
        CharacterDTO characterDTO = new CharacterDTO();
        characterDTO.setFullName("New character");

        Character characterEntity = mapper.characterDTOToCharacter(characterDTO);

        Assertions.assertEquals(characterDTO.getFullName(), characterEntity.getFullName());

    }

}
