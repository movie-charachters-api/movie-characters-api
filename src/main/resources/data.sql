/* Franchise */

/** lord of the rings: the return of the king */
INSERT INTO franchise(description, name)
VALUES ('The Lord of the Rings is a series of three epic fantasy adventure films directed by Peter Jackson, based on the novel written by J. R. R. Tolkien.',
        'lord of the rings');

/* Movies */
INSERT INTO movie(director, genre, picture, release_year, title, trailer)
VALUES (' Frank Darabont', 'drama', 'https://mediacdn.aent-m.com/prod-img/500/58/4049158-2783238.jpg?ae=2401476150',
        1999, 'the green mile', 'https://www.youtube.com/watch?v=Ki4haFrqSrw');
INSERT INTO movie(director, genre, picture, release_year, title, trailer, franchise_id)
VALUES ('Peter Jackson', 'Fantasy/Adventure',
        'https://dvd-shoppen.com/2970-large_default/sagan-om-konungens-aterkomst-2-disc.jpg', 2003,
        'lord of the rings: the return of the king', 'https://www.youtube.com/watch?v=1zPLzpqjO4U', 1);
INSERT INTO movie(director, genre, picture, release_year, title, trailer)
VALUES ('Paul Thomas Anderson', 'Drama/historic', 'https://i.ytimg.com/vi/0FIm5ATyAY0/maxresdefault.jpg', 2007,
        'There will be blood', 'https://www.youtube.com/watch?v=0FIm5ATyAY0');

/* Characters */

/** the green mile */
INSERT INTO character(alias, full_name, gender, picture)
VALUES ('Paul Ed', 'Tom Hanks', 'male',
        'https://en.wikipedia.org/wiki/Tom_Hanks#/media/File:Tom_Hanks,_February_2004.jpg');

INSERT INTO character(alias, full_name, gender, picture)
VALUES ('John Coffey', 'Michael Clarke Duncan', 'male',
        'https://new.static.tv.nu/19465225?forceFit=0&height=400&quality=50&width=400');

/** lord of the rings: the return of the king */
INSERT INTO character(alias, full_name, gender, picture)
VALUES ('Gimli ', 'john rhys-davies', 'male',
        'https://static.independent.co.uk/s3fs-public/thumbnails/image/2017/11/16/09/lotr-gimli.jpg');

/** There will be blood */
INSERT INTO character(alias, full_name, gender, picture)
VALUES ('Daniel Plainview ', ' Daniel Day-Lewis ', 'male',
        'https://nofilmschool.com/sites/default/files/styles/article_superwide/public/1489720882291-therewillbeblood.jpeg?itok=55WrwcQ5');

/* link character to movie */

/** green mile */
INSERT INTO movie_characters(movie_id, characters_character_id)
VALUES (1, 1);
INSERT INTO movie_characters(movie_id, characters_character_id)
VALUES (1, 2);

/** lord of the rings: the return of the king */
INSERT INTO movie_characters(movie_id, characters_character_id)
VALUES (2, 3);

/** there will be blood */
INSERT INTO movie_characters(movie_id, characters_character_id)
VALUES (3, 4);

/* Alter constraints */

/** when deleting franchise that is connected to a movie -> make franchise null in movie table */
alter table movie
drop constraint fke6bia0emn9rgc6498fspevei;

alter table movie
    add constraint fke6bia0emn9rgc6498fspevei
        foreign key (franchise_id) references franchise
            on delete set null;


/** when deleting movie that is connected to a characters (movie_characters)-> make movie null in movie_characters table */

alter table movie_characters
drop constraint fkdq7l2xdr2sg2xy6svy09qaa7m;

alter table movie_characters
    add constraint fkdq7l2xdr2sg2xy6svy09qaa7m
        foreign key (movie_id) references movie
            on delete set null;


/** when deleting movie that is connected to a characters (movie_characters)-> make movie null in movie_characters table */

alter table movie_characters
drop constraint fkh7l1mov7xswpfmjo6xgu0jm21;

alter table movie_characters
    add constraint fkh7l1mov7xswpfmjo6xgu0jm21
        foreign key (characters_character_id) references character
            on delete set null;

