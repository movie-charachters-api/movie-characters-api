package se.experis.moviecharactersapi;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MovieCharactersApiApplication{

    private final static Logger LOG = LoggerFactory
            .getLogger(MovieCharactersApiApplication.class);

    public static void main(String[] args) {
        SpringApplication.run(MovieCharactersApiApplication.class, args);
    }

}
