package se.experis.moviecharactersapi.service.franchise;

import se.experis.moviecharactersapi.model.Franchise;
import se.experis.moviecharactersapi.service.CrudService;

public interface FranchiseService extends CrudService<Franchise, Long> {
}
