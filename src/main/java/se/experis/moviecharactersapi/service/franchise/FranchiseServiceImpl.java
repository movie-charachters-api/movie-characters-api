package se.experis.moviecharactersapi.service.franchise;

import org.springframework.stereotype.Service;
import se.experis.moviecharactersapi.model.Franchise;
import se.experis.moviecharactersapi.repository.FranchiseRepository;

import java.util.Collection;

@Service
public class FranchiseServiceImpl implements FranchiseService{

    private final FranchiseRepository franchiseRepository;

    public FranchiseServiceImpl(FranchiseRepository franchiseRepository) {
        this.franchiseRepository = franchiseRepository;
    }

    @Override
    public Franchise findById(Long aLong) {
        return franchiseRepository.findById(aLong).get();
    }

    @Override
    public Collection<Franchise> findAll() {

        System.out.println("findAll: "  + franchiseRepository.findAll().stream().toString());

        return franchiseRepository.findAll();
    }

    @Override
    public Franchise add(Franchise entity) {
        return franchiseRepository.save(entity);
    }

    @Override
    public Franchise update(Franchise entity) {
        System.out.println("Franchise: " + entity);
        return franchiseRepository.save(entity);
    }

    @Override
    public void deleteById(Long aLong) {
        franchiseRepository.deleteById(aLong);
    }

    @Override
    public boolean exists(Long aLong) {
        return franchiseRepository.existsById(aLong);
    }
}
