package se.experis.moviecharactersapi.service.movie;

import org.springframework.stereotype.Service;
import se.experis.moviecharactersapi.model.Movie;
import se.experis.moviecharactersapi.repository.MovieRepository;

import java.util.Collection;

@Service
public class MovieServiceImpl implements MovieService {


    private final MovieRepository movieRepository;

    public MovieServiceImpl(MovieRepository movieRepository) {
        this.movieRepository = movieRepository;
    }


    @Override
    public Movie findById(Long aLong) {
        return movieRepository.findById(aLong).get();
    }

    @Override
    public Collection<Movie> findAll() {
        return movieRepository.findAll();
    }

    @Override
    public Movie add(Movie entity) {
        return movieRepository.save(entity);
    }

    @Override
    public Movie update(Movie entity) {
        return movieRepository.save(entity);
    }

    @Override
    public void deleteById(Long aLong) {
        movieRepository.deleteById(aLong);
    }

    @Override
    public boolean exists(Long aLong) {
        return movieRepository.existsById(aLong);
    }
}
