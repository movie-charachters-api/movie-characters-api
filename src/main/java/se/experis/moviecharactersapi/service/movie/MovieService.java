package se.experis.moviecharactersapi.service.movie;

import se.experis.moviecharactersapi.model.Movie;
import se.experis.moviecharactersapi.service.CrudService;

public interface MovieService extends CrudService<Movie, Long> {

}
