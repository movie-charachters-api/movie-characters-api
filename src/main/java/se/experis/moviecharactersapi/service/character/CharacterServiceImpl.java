package se.experis.moviecharactersapi.service.character;

import org.springframework.stereotype.Service;
import se.experis.moviecharactersapi.model.Character;
import se.experis.moviecharactersapi.repository.CharacterRepository;

import java.util.Collection;

@Service
public class CharacterServiceImpl implements CharacterService {

    private final CharacterRepository characterRepository;

    public CharacterServiceImpl(CharacterRepository characterRepository) {
        this.characterRepository = characterRepository;
    }

    @Override
    public Character findById(Long aLong) {
        return characterRepository.findById(aLong).get();
    }

    @Override
    public Collection<Character> findAll() {
        return characterRepository.findAll();
    }

    @Override
    public Character add(Character entity) {
        return characterRepository.save(entity);
    }

    @Override
    public Character update(Character entity) {
        return characterRepository.save(entity);
    }

    @Override
    public void deleteById(Long id) {
        characterRepository.deleteById(id);
    }

    @Override
    public boolean exists(Long id) {
        return characterRepository.existsById(id);
    }
}
