package se.experis.moviecharactersapi.service.character;

import se.experis.moviecharactersapi.model.Character;
import se.experis.moviecharactersapi.service.CrudService;

public interface CharacterService extends CrudService<Character, Long> {

}
