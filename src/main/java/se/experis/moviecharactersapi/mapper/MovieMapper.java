package se.experis.moviecharactersapi.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.springframework.beans.factory.annotation.Autowired;
import se.experis.moviecharactersapi.dto.franchise.FranchiseDTO;
import se.experis.moviecharactersapi.dto.movie.MovieDTO;
import se.experis.moviecharactersapi.model.Franchise;
import se.experis.moviecharactersapi.model.Movie;
import se.experis.moviecharactersapi.model.Character;
import se.experis.moviecharactersapi.service.character.CharacterService;
import se.experis.moviecharactersapi.service.franchise.FranchiseService;
import se.experis.moviecharactersapi.service.movie.MovieService;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring")
public abstract class MovieMapper {
    @Autowired
    protected CharacterService characterService;
    @Autowired
    protected FranchiseService franchiseService;

    @Mapping(target = "characters", source = "characters", qualifiedByName = "characterToIds")
    @Mapping(target = "franchise", source = "franchise.id")
    public abstract MovieDTO movieToMovieDTO(Movie movie);

    public abstract Collection<MovieDTO> movieToMovieDTO(Collection<Movie> movies);

    // -----

    @Mapping(target = "characters", source = "characters", qualifiedByName = "characterIdsToCharacters")
    @Mapping(target = "franchise", source = "franchise", qualifiedByName = "franchiseIdsToFranchises")
    public abstract Movie movieDTOToMovie(MovieDTO dto);

    @Named("characterIdsToCharacters")
    Set<Character> mapIdsToCharacters(Set<Long> id) {
        return id.stream()
                .map(i -> characterService.findById(i))
                .collect(Collectors.toSet());
    }

    @Named("franchiseIdsToFranchises")
    Franchise mapIdsToFranchises(Long id) {
        return franchiseService.findById(id);
    }
    @Named("characterToIds")
    Set<Long> mapCharactersToIds(Set<Character> source) {
        if (source == null)
            return null;
        return source.stream()
                .map(s -> s.getId()).collect(Collectors.toSet());
    }



}
