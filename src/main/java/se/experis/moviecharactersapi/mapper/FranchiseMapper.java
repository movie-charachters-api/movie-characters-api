package se.experis.moviecharactersapi.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.springframework.beans.factory.annotation.Autowired;
import se.experis.moviecharactersapi.dto.character.CharacterDTO;
import se.experis.moviecharactersapi.dto.franchise.FranchiseDTO;
import se.experis.moviecharactersapi.dto.movie.MovieDTO;
import se.experis.moviecharactersapi.model.Character;
import se.experis.moviecharactersapi.model.Franchise;
import se.experis.moviecharactersapi.model.Movie;
import se.experis.moviecharactersapi.service.character.CharacterService;
import se.experis.moviecharactersapi.service.franchise.FranchiseService;
import se.experis.moviecharactersapi.service.movie.MovieService;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring")
public abstract class FranchiseMapper {

    @Autowired
    protected FranchiseService franchiseService;
    @Autowired
    protected MovieService movieService;

    @Mapping(target = "movies", source = "movies", qualifiedByName = "movieToIds")
    public abstract FranchiseDTO franchiseToFranchiseDTO(Franchise franchise);

    public abstract Collection<FranchiseDTO> franchiseToFranchiseDTO(Collection<Franchise> franchises);

    @Mapping(target = "movies", source = "movies", qualifiedByName = "movieIdsToMovies")
    public abstract Franchise franchiseDTOToFranchise(FranchiseDTO dto);

    @Named("movieIdsToMovies")
    Set<Movie> mapIdsToMovies(Set<Long> id) {
        System.out.println("id:" + id);
        return id.stream()
                .map( i -> movieService.findById(i))
                .collect(Collectors.toSet());
    }

    @Named("movieToIds")
    Set<Long> mapMoviesToIds(Set<Movie> source) {
        if(source == null)
            return null;
        return source.stream()
                .map(s -> s.getId()).collect(Collectors.toSet());
    }


}
