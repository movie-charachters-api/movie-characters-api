package se.experis.moviecharactersapi.mapper;

import org.mapstruct.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import se.experis.moviecharactersapi.dto.character.CharacterDTO;
import se.experis.moviecharactersapi.model.Character;
import se.experis.moviecharactersapi.service.character.CharacterService;

@Mapper(componentModel = "spring")
public abstract class CharacterMapper {

    @Autowired
    protected CharacterService characterService;

    public abstract CharacterDTO characterToCharacterDTO(Character character);

    public abstract Character characterDTOToCharacter(CharacterDTO dto);

}
