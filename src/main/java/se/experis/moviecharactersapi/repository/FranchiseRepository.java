package se.experis.moviecharactersapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import se.experis.moviecharactersapi.model.Franchise;

@Repository
public interface FranchiseRepository extends JpaRepository<Franchise, Long>{
}
