package se.experis.moviecharactersapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import se.experis.moviecharactersapi.model.Character;

@Repository
public interface CharacterRepository extends JpaRepository<Character, Long> {

}
