package se.experis.moviecharactersapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import se.experis.moviecharactersapi.model.Movie;

@Repository
public interface MovieRepository extends JpaRepository<Movie, Long> {
}
