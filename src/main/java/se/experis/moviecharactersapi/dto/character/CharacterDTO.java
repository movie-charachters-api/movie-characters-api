package se.experis.moviecharactersapi.dto.character;

import lombok.Data;

@Data
public class CharacterDTO {

    private Long id;
    private String fullName;
    private String alias;
    private String gender;
    private String picture;

}