package se.experis.moviecharactersapi.dto.franchise;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import java.util.Set;

@Data
public class FranchiseDTO {

    private Long id;
    private String name;
    private String description;


    /* Relational Mapping */
    @JsonIgnore
    private Set<Long> movies;
}
