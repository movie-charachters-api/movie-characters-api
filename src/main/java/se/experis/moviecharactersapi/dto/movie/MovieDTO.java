package se.experis.moviecharactersapi.dto.movie;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import java.util.Set;

@Data
public class MovieDTO {
    private Long id;
    private String title;
    private String genre;
    private int releaseYear;
    private String director;
    private String picture;
    private String trailer;

    /* Relational Mapping */

    @JsonIgnore
    Set<Long> characters;
    @JsonIgnore
    private Long franchise;
}
