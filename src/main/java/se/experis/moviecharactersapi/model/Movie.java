package se.experis.moviecharactersapi.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;

@Entity
@Getter
@Setter
@Table(name="movie")
public class Movie {

    /* Variables */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(nullable = false, length = 50)
    private String title;
    @Column(length = 50)
    private String genre;
    @Column(nullable = false)
    private int releaseYear;
    @Column(length = 50)
    private String director;
    private String picture;
    private String trailer;

    /* Relational Mapping */

    @ManyToMany
    Set<Character> characters;

    @ManyToOne
    @JoinColumn(name = "franchise_id")
    private Franchise franchise;
}
