package se.experis.moviecharactersapi.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;

@Entity
@Getter
@Setter
public class Franchise {

    /* Variables */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "franchise_id")
    private Long id;
    @Column(nullable = false, length = 50)
    private String name;
    private String description;


    /* Relational Mapping */

    @OneToMany(mappedBy = "franchise")
    private Set<Movie> movies;


}
