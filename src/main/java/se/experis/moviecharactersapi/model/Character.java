package se.experis.moviecharactersapi.model;

import com.fasterxml.jackson.annotation.JsonGetter;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
public class Character {

    /* Variables */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "character_id")
    private Long id;
    @Column(nullable = false, length = 50)
    private String fullName;
    @Column(length = 50)
    private String alias;
    @Column(length = 10)
    private String gender;
    private String picture;

}
