package se.experis.moviecharactersapi.controller.franchise;

import com.sun.xml.bind.v2.TODO;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import se.experis.moviecharactersapi.dto.franchise.FranchiseDTO;
import se.experis.moviecharactersapi.mapper.FranchiseMapper;
import se.experis.moviecharactersapi.model.Franchise;
import se.experis.moviecharactersapi.service.franchise.FranchiseService;
import se.experis.moviecharactersapi.service.franchise.FranchiseServiceImpl;

import java.util.Collection;

@RestController
@RequestMapping(path = "api/v1/franchise")
public class FranchiseController {
    private final FranchiseServiceImpl franchiseServiceImpl;
    private final FranchiseService franchiseService;
    private final FranchiseMapper franchiseMapper;

    public FranchiseController(FranchiseServiceImpl franchiseServiceImpl, FranchiseService franchiseService, FranchiseMapper franchiseMapper) {
        this.franchiseServiceImpl = franchiseServiceImpl;
        this.franchiseService = franchiseService;
        this.franchiseMapper = franchiseMapper;
    }

    @GetMapping
    public ResponseEntity findAll() {
        Collection<FranchiseDTO> studs = franchiseMapper.franchiseToFranchiseDTO(
                franchiseService.findAll()
        );
        return ResponseEntity.ok(studs);

    }

    @PostMapping
    public ResponseEntity createFranchise(@RequestBody Franchise franchise){
        franchiseService.add(franchise);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @GetMapping("{id}")
    public ResponseEntity findFranchiseById(@PathVariable Long id){
        FranchiseDTO franchiseDTO = franchiseMapper.franchiseToFranchiseDTO(franchiseServiceImpl.findById(id));
        return ResponseEntity.ok(franchiseDTO);
    }

    @PutMapping("{id}")
    public ResponseEntity updateFranchise(@RequestBody FranchiseDTO franchiseDTO, @PathVariable Long id){
        if (!franchiseServiceImpl.exists(id)) {
            System.out.println("error");
            return ResponseEntity.notFound().build();
        } else {
            System.out.println(franchiseDTO);
            franchiseServiceImpl.update(franchiseMapper.franchiseDTOToFranchise(franchiseDTO));
            return ResponseEntity.noContent().build();
        }
    }
    @DeleteMapping("{id}")
    public ResponseEntity deleteFranchise(@PathVariable Long id){
        if(!franchiseServiceImpl.exists(id)){
            return ResponseEntity.notFound().build();
        }else {
            franchiseServiceImpl.deleteById(id);
            return ResponseEntity.noContent().build();
        }
    }
}
