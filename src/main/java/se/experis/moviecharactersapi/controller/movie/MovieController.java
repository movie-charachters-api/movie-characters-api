package se.experis.moviecharactersapi.controller.movie;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import se.experis.moviecharactersapi.dto.franchise.FranchiseDTO;
import se.experis.moviecharactersapi.dto.movie.MovieDTO;
import se.experis.moviecharactersapi.mapper.MovieMapper;
import se.experis.moviecharactersapi.model.Movie;
import se.experis.moviecharactersapi.service.movie.MovieService;
import se.experis.moviecharactersapi.service.movie.MovieServiceImpl;

import java.util.Collection;

@RestController
@RequestMapping(path = "api/v1/movie")
public class MovieController {

    private final MovieServiceImpl movieServiceImpl;
    private final MovieService movieService;
    private final MovieMapper movieMapper;


    public MovieController(MovieServiceImpl movieServiceImpl, MovieService movieService, MovieMapper movieMapper) {
        this.movieServiceImpl = movieServiceImpl;
        this.movieService = movieService;
        this.movieMapper = movieMapper;
    }

    @GetMapping
    public ResponseEntity findAll(){
        Collection<MovieDTO> studs = movieMapper.movieToMovieDTO(
                movieService.findAll()
        );
        return ResponseEntity.ok(studs);
    }

    @PostMapping
    public ResponseEntity createMovie(@RequestBody Movie movie){
        movieService.add(movie);
        return ResponseEntity.status(HttpStatus.CREATED).build();
        //todo: json ignore ,character franchise
    }
}
