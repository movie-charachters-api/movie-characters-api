package se.experis.moviecharactersapi.controller.character;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import se.experis.moviecharactersapi.dto.character.CharacterDTO;
import se.experis.moviecharactersapi.mapper.CharacterMapper;
import se.experis.moviecharactersapi.model.Character;
import se.experis.moviecharactersapi.service.character.CharacterService;
import se.experis.moviecharactersapi.service.character.CharacterServiceImpl;

import java.util.Collection;

@RestController
@RequestMapping(path = "api/v1/character")
public class CharacterController {

    private final CharacterService characterService;

    private final CharacterServiceImpl characterServiceImpl;
    private final CharacterMapper characterMapper;


    public CharacterController(CharacterService characterService, CharacterMapper characterMapper, CharacterServiceImpl characterServiceImpl) {
        this.characterService = characterService;
        this.characterMapper = characterMapper;
        this.characterServiceImpl = characterServiceImpl;
    }

    @GetMapping
    public ResponseEntity<Collection<Character>> findAll() {
        return ResponseEntity.ok(characterService.findAll());
    }

    @PostMapping
    public ResponseEntity createCharacter(@RequestBody Character character) {
        characterService.add(character);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    // Todo: fix API responses
    /*
    @Operation(summary = "Get a character by ID")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = CharacterDTO.class)) }),
            @ApiResponse(responseCode = "500",
                    description = "Character does not exist with supplied ID",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class)) })
    })
*/
    @GetMapping("{id}")
    public ResponseEntity findCharacterById(@PathVariable Long id) {

        CharacterDTO characterDTO = characterMapper.characterToCharacterDTO(characterServiceImpl.findById(id));
        return ResponseEntity.ok(characterDTO);

    }

    @PutMapping("{id}")
    public ResponseEntity updateCharacter(@RequestBody CharacterDTO characterDTO, @PathVariable Long id) {

        if (!characterServiceImpl.exists(id)) {
            return ResponseEntity.notFound().build();
        } else {
            characterServiceImpl.update(characterMapper.characterDTOToCharacter(characterDTO));
            return ResponseEntity.noContent().build();
        }
    }

    @DeleteMapping("{id}")
    public ResponseEntity deleteCharacter(@PathVariable Long id) {

        if (!characterServiceImpl.exists(id)) {
            return ResponseEntity.notFound().build();
        } else {
            characterServiceImpl.deleteById(id);
            return ResponseEntity.noContent().build();
        }
    }


}
