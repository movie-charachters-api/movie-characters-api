package se.experis.moviecharactersapi;

import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;
import se.experis.moviecharactersapi.repository.CharacterRepository;
import se.experis.moviecharactersapi.service.character.CharacterServiceImpl;

@Component
public class Runner implements ApplicationRunner {
    private final CharacterRepository characterRepository;
    CharacterServiceImpl characterService;

    public Runner(CharacterRepository characterRepository, CharacterServiceImpl characterService) {
        this.characterRepository = characterRepository;
        this.characterService = characterService;
    }

    @Override
    public void run(ApplicationArguments args) {

    }
}
